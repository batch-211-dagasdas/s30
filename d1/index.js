db.fruits.insertMany([
{
name : "Apple",
color : "Red",
stock : 20,
price: 40,
supplier_id : 1,
onSale : true,
origin: [ "Philippines", "US" ]
},
{
name : "Banana",
color : "Yellow",
stock : 15,
price: 20,
supplier_id : 2,
onSale : true,
origin: [ "Philippines", "Ecuador" ]
},
{
name : "Kiwi",
color : "Green",
stock : 25,
price: 50,
supplier_id : 1,
onSale : true,
origin: [ "US", "China" ]
},
{
name : "Mango",
color : "Yellow",
stock : 10,
price: 120,
supplier_id : 2,
onSale : false,
origin: [ "Philippines", "India" ]
}
]);

// MongoDB Aggregation
/*
	- used to generate manupulate data and perform operations to create filtered results that help in analyzing data
	-compared, aggregation gives us access to manipulate, filter, and compute for results, provideing us w/ info to make nessary development decisions
	-aggregate in mongoDB are very flexible and can form your own aggregateion pipeline depending on the need of your application

*/
// AGREGATE Methods
/*
	1.MATCH: " $match"
		-the "$match" is used to pass the doc that meet the specified condition to the next pipeline stage/agragation process
			syntax:
				{$match: { field: value}}
*/
db.fruits.aggregate([
{$match:{onSale:true, stock: { $gte:20} }}
])

db.fruits.aggregate([
{$match: {stock: { $gte:20} } }
]);


/* 2. GROUP: "$group"
	-The "$group" is used to group elements together field-value pairs using the data from frouped elements
		Syntax:
			{$group: {_id: "Value", fieldResult: "ValueResult"}}

*/
db.fruits.aggregate([
{ $group: { _id: "$supplier_id", total: { $sum: "$stock" } } }
]);


/* 3.SORT: "$sort"
		-The "$sort" can be used when changing th order of aggregated results
		-providing the value of -1 will soth the doc in a descending order
		-providing the value of 1 will soth the doc in a ascending order
		Syntax:
			{$sort:{field: 1/1}}
*/
db.fruits.aggregate([
	{$match: {onSale: true} },
	{$group: {_id: "$supplier_id", total:{$sum: "$stock"} } },
	{$sort: {total : -1}}
	]);

/* 4. PROJECT: "$project
		-the "$project" Can be used when aggregating data to include or exclue field th returned results
			syntax:
				{$project: {field : 1/0}}

*/
db.fruits.aggregate([

	{$match: {onSale: true }},
	{$group: {_id: "$supplier_id", total : {$sum: "$stock"}}},
	{$project: {_id: 0} }

	]);

db.fruits.aggregate([

	{$match: {onSale: true }},
	{$group: {_id: "$supplier_id", total : {$sum: "$stock"}}},
	{$project: {_id: 0} },
	{$sort: {total: -1}}

	]);

/*	AGGREGATION PIPELINES
		-The aggregation pipeline in MONGODB is a framework for the aggregation
		-each stage transform the document as they pass through the deadlines
			Syntax: db.collection.aggregate([
			{stageA},
			{stageB},
			{stageC}

			]);

*/

// OPERATORS
// #1 "$sum" -get the total of evething
db.fruits.aggregate([
	{$group:{_id: "$supplier_id", total: {$sum:"$stock"}}}
]);

// 2. "$max" -gets the highest value of everything else
db.fruits.aggregate([
	{$group: {_id: "supplier_id", max_price: {$max : "$price"}}}
	]);
// 3. "$min"-get the lowest value of everything else
db.fruits.aggregate([
	{$group: {_id: "$supplier_id", min_price: {$min : "$stock"}}}
	]);
// 4. "$avg"-get the average value of all the fields
db.fruits.aggregate([
	{$group: {_id: "$supplier_id", avg_price: {$avg : "$price"}}}
	]);



db.fruits.aggregate([

	{$match: {onSale: true }},
	{$group: {_id: "supplier_id", total : {$sum: "$stock"}}},
	]);

db.fruits.aggregate([
{$match:{onSale:true,}}, {total : {$sum: "$stock"}}
])


db.fruits.aggregate([

	{$match: {onSale: true }},
	{ $group: {_id: "$supplier_id",stocksOnSale: {$count: {}}}
   }
	
	]);

