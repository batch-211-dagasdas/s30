
// 1
db.fruits.aggregate([

	{$match: {onSale: true, stocksOnSale: {$count: {}}}},
	// { $group: {_id: "supplier_id",stocksOnSale: {$count: {}}}
 //   }
	
	]);

// 2

db.fruits.aggregate([

	{$match:{stock: { $gte:20} }},
	{$group: {_id: "$supplier_id",stocksOnSale: {$count: {}}}}
	
	]);

// Ava
db.fruits.aggregate([
	{$match: {onSale: true }},
	{$group: {_id: "$supplier_id", avg_price: {$avg : "$price"}}}
	]);

// Highest Price per supplier
db.fruits.aggregate([
	{$group: {_id: "$supplier_id", max_price: {$max : "$price"}}}
	]);

// Lowest Price
db.fruits.aggregate([
	{$group: {_id: "$supplier_id", lowest_price: {$min : "$price"}}}
	]);

